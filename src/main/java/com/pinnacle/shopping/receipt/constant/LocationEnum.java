package com.pinnacle.shopping.receipt.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LocationEnum {

  CALIFORNIA("CA", "California", 9.75), //
  NEW_YORK("NY", "New York", 8.875);

  private final String code;

  private final String name;

  private final double taxRate;

}
