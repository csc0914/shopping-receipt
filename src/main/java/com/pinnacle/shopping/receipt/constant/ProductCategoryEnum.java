package com.pinnacle.shopping.receipt.constant;

public enum ProductCategoryEnum {

  FOOD, CLOTHING, STATIONERY;

}
