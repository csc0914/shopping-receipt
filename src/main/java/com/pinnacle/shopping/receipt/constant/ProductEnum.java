package com.pinnacle.shopping.receipt.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductEnum {

  BOOK("book", ProductCategoryEnum.STATIONERY), //
  POTATO_CHIPS("potato chips", ProductCategoryEnum.FOOD), //
  PENCILS("pencils", ProductCategoryEnum.STATIONERY), //
  SHIRT("shirt", ProductCategoryEnum.CLOTHING);


  private final String name;

  private final ProductCategoryEnum category;

  public boolean isStationery() {
    return ProductCategoryEnum.STATIONERY.equals(this.category);
  }

  public boolean isFood() {
    return ProductCategoryEnum.FOOD.equals(this.category);
  }

  public boolean isClothing() {
    return ProductCategoryEnum.CLOTHING.equals(this.category);
  }


}
