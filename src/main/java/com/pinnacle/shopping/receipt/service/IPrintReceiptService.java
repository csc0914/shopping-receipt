package com.pinnacle.shopping.receipt.service;

import com.pinnacle.shopping.receipt.model.Receipt;

/**
 * Print the receipt in specified format with the list of items customer purchased, including
 * name/qty/price, subtotal, sales tax, and total
 * 
 * @author Chun
 */
public interface IPrintReceiptService {

  public String print(Receipt receipt);
}
