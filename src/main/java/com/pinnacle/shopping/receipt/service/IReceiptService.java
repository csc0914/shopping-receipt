package com.pinnacle.shopping.receipt.service;

import java.util.List;
import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.exception.EmptyListException;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;


/**
 * Mainly used to generate the receipt {@link Receipt}
 * 
 * @author Chun
 */
public interface IReceiptService {

  public Receipt getReceipt(List<PurchaseProduct> purchaseProductList) throws EmptyListException;

  public LocationEnum getLocation();

}
