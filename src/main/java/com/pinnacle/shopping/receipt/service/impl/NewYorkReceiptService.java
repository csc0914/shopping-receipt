package com.pinnacle.shopping.receipt.service.impl;

import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import org.springframework.stereotype.Service;

@Service
public class NewYorkReceiptService extends AbstractReceiptService {

  @Override
  public LocationEnum getLocation() {
    return LocationEnum.NEW_YORK;
  }

  @Override
  public boolean isExempt(PurchaseProduct purchaseProduct) {
    ProductEnum product = purchaseProduct.getProduct();
    return product.isFood() || product.isClothing();
  }

}
