package com.pinnacle.shopping.receipt.service.impl;

import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import org.springframework.stereotype.Service;

@Service
public class CaliforniaReceiptService extends AbstractReceiptService {

  @Override
  public LocationEnum getLocation() {
    return LocationEnum.CALIFORNIA;
  }

  @Override
  public boolean isExempt(PurchaseProduct product) {
    return product.getProduct().isFood();
  }

}
