package com.pinnacle.shopping.receipt.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import com.pinnacle.shopping.receipt.exception.EmptyListException;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import com.pinnacle.shopping.receipt.service.IReceiptService;
import com.pinnacle.shopping.receipt.util.TaxUtils;


/**
 * 
 * Abstract class to do the main logic for generating receipt
 * 
 * @author Chun
 */
public abstract class AbstractReceiptService implements IReceiptService {


  public Receipt getReceipt(List<PurchaseProduct> purchaseProductList) throws EmptyListException {
    return Optional.ofNullable(purchaseProductList).map(this::initReceipt)
        .orElseThrow(() -> new EmptyListException("The product list cannot be empty"));
  }

  private Receipt initReceipt(List<PurchaseProduct> purchaseProductList) {
    return new Receipt(purchaseProductList, getSubTotal(purchaseProductList),
        getTax(purchaseProductList));
  }

  private double getSubTotal(List<PurchaseProduct> purchaseProductList) {
    return purchaseProductList.stream().reduce(0d, this::getSubTotal, Double::sum);
  }

  private double getTax(List<PurchaseProduct> purchaseProductList) {
    return purchaseProductList.stream()

        .filter(Predicate.not(this::isExempt))

        .reduce(0d, this::getTotalTax, Double::sum);
  }

  private double getSubTotal(double subTotal, PurchaseProduct purchaseProduct) {
    return subTotal + getProductTotalPrice(purchaseProduct);
  }

  private double getTotalTax(double totalTax, PurchaseProduct purchaseProduct) {
    return totalTax + getProductTotalTax(purchaseProduct);
  }

  private double getProductTotalTax(PurchaseProduct purchaseProduct) {
    return TaxUtils.getTax(getProductTotalPrice(purchaseProduct) * getTaxRate());
  }


  private double getTaxRate() {
    return getLocation().getTaxRate() / 100;
  }

  private double getProductTotalPrice(PurchaseProduct purchaseProduct) {
    return purchaseProduct.getQuantity() * purchaseProduct.getPrice();
  }


  /**
   * Check if the product is exempt
   * 
   * @param product
   * @return true if it is exempt
   */
  protected abstract boolean isExempt(PurchaseProduct product);



}
