package com.pinnacle.shopping.receipt.service.impl;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import com.pinnacle.shopping.receipt.service.IPrintReceiptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class PrintReceiptService implements IPrintReceiptService {

  @Autowired
  private MessageSource messageSource;

  private static final String LINE_BREAK = "\n";

  @Override
  public String print(Receipt receipt) {

    StringBuilder sb = new StringBuilder();

    printHeader(sb);

    receipt.getPurchaseProductList().stream()
        .forEach(purchaseProduct -> printItem(sb, purchaseProduct));

    printFooter(sb, receipt);

    return sb.toString();
  }

  private void printHeader(StringBuilder sb) {
    addLineBreak(sb);
    sb.append(messageSource.getMessage("receipt.header", null, Locale.getDefault()));
    addLineBreak(sb);
  }

  private void printItem(StringBuilder sb, PurchaseProduct purchaseProduct) {

    ProductEnum product = purchaseProduct.getProduct();

    sb.append(messageSource.getMessage("receipt.item",
        new Object[] {product.getName(), purchaseProduct.getPrice(), purchaseProduct.getQuantity()},
        Locale.getDefault()));

    addLineBreak(sb);
  }


  private void printFooter(StringBuilder sb, Receipt receipt) {
    sb.append(messageSource.getMessage("receipt.footer",

        new Object[] {getPrice(receipt.getSubTotal()), getPrice(receipt.getTax()),
            getPrice(receipt.getSubTotal() + receipt.getTax())},

        Locale.getDefault()));
  }

  private void addLineBreak(StringBuilder sb) {
    sb.append(LINE_BREAK);
  }

  private String getPrice(double price) {
    DecimalFormat priceFormat = new DecimalFormat("0.00#");
    priceFormat.setRoundingMode(RoundingMode.HALF_UP);
    return priceFormat.format(price);
  }


}
