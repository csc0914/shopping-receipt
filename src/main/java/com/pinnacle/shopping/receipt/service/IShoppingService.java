package com.pinnacle.shopping.receipt.service;

import java.util.List;
import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;

/**
 * Mainly used to generate the receipt and print the list of items customer purchased, including
 * name/qty/price, subtotal, sales tax, and total,
 * 
 * @author Chun
 */
public interface IShoppingService {

  /**
   * Generate the receipt and print the list of items customer purchased, including name/qty/price,
   * subtotal, sales tax, and total,
   * 
   * @param location is used to find out which service {@link IReceiptService} is suitable to
   *        generate the receipt by comparing with {@link IReceiptService#getLocation()}
   * 
   * @param purchaseProductList
   */
  public void checkout(LocationEnum location, List<PurchaseProduct> purchaseProductList);
}
