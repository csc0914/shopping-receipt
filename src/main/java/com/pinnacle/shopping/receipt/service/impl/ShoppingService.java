package com.pinnacle.shopping.receipt.service.impl;

import java.util.List;
import java.util.Optional;
import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.exception.EmptyListException;
import com.pinnacle.shopping.receipt.exception.NoServiceProvidedException;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import com.pinnacle.shopping.receipt.service.IPrintReceiptService;
import com.pinnacle.shopping.receipt.service.IReceiptService;
import com.pinnacle.shopping.receipt.service.IShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ShoppingService implements IShoppingService {

  @Autowired
  public List<IReceiptService> receiptServiceList;

  @Autowired
  private IPrintReceiptService outputService;

  @Override
  public void checkout(LocationEnum location, List<PurchaseProduct> purchaseProductList) {
    Optional<IReceiptService> receiptService = receiptServiceList.stream()
        .filter(service -> service.getLocation().equals(location)).findFirst();

    receiptService.ifPresentOrElse(
        theReceiptService -> this.checkout(theReceiptService, purchaseProductList), () -> {
          throw new NoServiceProvidedException("There is no service provided for "
              + location.getName() + ", please create it with implement the type IReceiptService");
        });


  }

  private void checkout(IReceiptService receiptService, List<PurchaseProduct> purchaseProductList) {
    try {
      Receipt receipt = receiptService.getReceipt(purchaseProductList);
      log.info(outputService.print(receipt));
    } catch (EmptyListException e) {
      log.error(e);
    }
  }
}
