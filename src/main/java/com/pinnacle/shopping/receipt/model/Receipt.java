package com.pinnacle.shopping.receipt.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Receipt {

  private List<PurchaseProduct> purchaseProductList;
  private double subTotal;
  private double tax;

  public double getTotal() {
    return subTotal + tax;
  }

}
