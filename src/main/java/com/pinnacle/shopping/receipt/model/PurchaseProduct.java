package com.pinnacle.shopping.receipt.model;

import com.pinnacle.shopping.receipt.constant.ProductEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * Store the purcahsed product with price and quantity
 * 
 * @author Chun
 */
@Data
@AllArgsConstructor
public class PurchaseProduct {

  private ProductEnum product;
  private double price;
  private int quantity;
}
