package com.pinnacle.shopping.receipt.util;


/**
 * Mainly used to calculate the tax which should be rounded up to the nearest 0.05
 * 
 * 
 * @author Chun
 */
public abstract class TaxUtils {

  private static final double NEAREST_DECIMAL = 1 / 0.05;

  public static double getTax(double tax) {
    return Math.ceil(tax * NEAREST_DECIMAL) / NEAREST_DECIMAL;
  }
}
