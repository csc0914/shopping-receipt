package com.pinnacle.shopping.receipt;

import java.util.Arrays;
import java.util.List;
import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.service.IShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import lombok.extern.log4j.Log4j2;

@SpringBootApplication(scanBasePackages = "com.pinnacle.shopping.receipt",
    exclude = {DataSourceAutoConfiguration.class})
@Log4j2
public class ShoppingReceiptApplication implements CommandLineRunner {

  @Autowired
  private IShoppingService shoppingService;

  public static void main(String[] args) {
    SpringApplication.run(ShoppingReceiptApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    useCase1();
    useCase2();
    useCase3();
  }

  private void useCase1() {
    log.info("===============Use case 1:====================");
    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.BOOK, 17.99, 1),
        new PurchaseProduct(ProductEnum.POTATO_CHIPS, 3.99, 1)

    );

    shoppingService.checkout(LocationEnum.CALIFORNIA, purchaseProductList);
  }

  private void useCase2() {
    log.info("===============Use case 2:====================");
    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.BOOK, 17.99, 1),

        new PurchaseProduct(ProductEnum.PENCILS, 2.99, 3));

    shoppingService.checkout(LocationEnum.NEW_YORK, purchaseProductList);
  }


  private void useCase3() {
    log.info("===============Use case 3:====================");
    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.PENCILS, 2.99, 2),
        new PurchaseProduct(ProductEnum.SHIRT, 29.99, 1));

    shoppingService.checkout(LocationEnum.NEW_YORK, purchaseProductList);
  }

}
