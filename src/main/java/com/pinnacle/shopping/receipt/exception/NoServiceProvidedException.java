package com.pinnacle.shopping.receipt.exception;

import com.pinnacle.shopping.receipt.constant.LocationEnum;
import com.pinnacle.shopping.receipt.service.IReceiptService;

/**
 * An excpetion toindicate that no service provided.
 * 
 * <p>
 * Example: if there is a new location provided in {@link LocationEnum}, suppose there is
 * implementation of {@link IReceiptService} for that location
 * 
 * @author Chun
 */
public class NoServiceProvidedException extends RuntimeException {

  public NoServiceProvidedException(String message) {
    super(message);
  }
}
