package com.pinnacle.shopping.receipt.exception;


/**
 * An excpetion to indicate the list is empty
 * 
 * @author Chun
 */
public class EmptyListException extends RuntimeException {

  public EmptyListException(String message) {
    super(message);
  }
}
