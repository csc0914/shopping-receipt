package com.pinnacle.shopping.receipt.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TaxUtilsTest {


  @Test
  public void getTax() {

    assertEquals(1.15, TaxUtils.getTax(1.13));

    assertEquals(1.20, TaxUtils.getTax(1.16));

    assertEquals(1.20, TaxUtils.getTax(1.151));

    assertEquals(1.75, TaxUtils.getTax(1.75));

  }
}
