package com.pinnacle.shopping.receipt.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Arrays;
import java.util.List;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PrintReceiptServiceTest {

  @Autowired
  private IPrintReceiptService printReceiptService;

  private static final String EXPECTED_OUTPUT =
      "\nitem		price		qty\nbook		$17.99		1\npencils		$2.99		3\nsubtotal:			$26.96\ntax:				$2.40\ntotal:				$29.36";


  @Test
  public void print_success() {

    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.BOOK, 17.99, 1),
        new PurchaseProduct(ProductEnum.PENCILS, 2.99, 3)

    );

    assertEquals(EXPECTED_OUTPUT,
        printReceiptService.print(new Receipt(purchaseProductList, 26.96, 2.4)));

  }
}
