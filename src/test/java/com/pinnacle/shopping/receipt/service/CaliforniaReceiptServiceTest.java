package com.pinnacle.shopping.receipt.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.Arrays;
import java.util.List;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.exception.EmptyListException;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import com.pinnacle.shopping.receipt.service.impl.CaliforniaReceiptService;
import org.decimal4j.util.DoubleRounder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CaliforniaReceiptServiceTest {

  @Autowired
  private CaliforniaReceiptService californiaReceiptService;

  @Test
  public void getReceiptWithExempt_success() throws EmptyListException {

    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.BOOK, 17.99, 1),
        new PurchaseProduct(ProductEnum.POTATO_CHIPS, 3.99, 1)

    );

    Receipt receipt = californiaReceiptService.getReceipt(purchaseProductList);

    assertEquals(21.98, DoubleRounder.round(receipt.getSubTotal(), 2));

    assertEquals(1.8, DoubleRounder.round(receipt.getTax(), 2));

    assertEquals(23.78, DoubleRounder.round(receipt.getTotal(), 2));

  }



  @Test
  public void getReceipt_emptyListExceptionThrow() throws EmptyListException {

    assertThrows(EmptyListException.class, () -> californiaReceiptService.getReceipt(null));

  }

}
