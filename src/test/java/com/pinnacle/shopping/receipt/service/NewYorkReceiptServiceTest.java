package com.pinnacle.shopping.receipt.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.Arrays;
import java.util.List;
import com.pinnacle.shopping.receipt.constant.ProductEnum;
import com.pinnacle.shopping.receipt.exception.EmptyListException;
import com.pinnacle.shopping.receipt.model.PurchaseProduct;
import com.pinnacle.shopping.receipt.model.Receipt;
import com.pinnacle.shopping.receipt.service.impl.NewYorkReceiptService;
import org.decimal4j.util.DoubleRounder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class NewYorkReceiptServiceTest {

  @Autowired
  private NewYorkReceiptService newYorkReceiptService;

  @Test
  public void getReceiptWithoutExempt_success() throws EmptyListException {

    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.BOOK, 17.99, 1),
        new PurchaseProduct(ProductEnum.PENCILS, 2.99, 3)

    );

    Receipt receipt = newYorkReceiptService.getReceipt(purchaseProductList);

    assertEquals(26.96, receipt.getSubTotal());

    assertEquals(2.4, DoubleRounder.round(receipt.getTax(), 2));

    assertEquals(29.36, DoubleRounder.round(receipt.getTotal(), 2));

  }

  @Test
  public void getReceiptWithExempt_success() throws EmptyListException {

    List<PurchaseProduct> purchaseProductList = Arrays.asList(

        new PurchaseProduct(ProductEnum.PENCILS, 2.99, 2),
        new PurchaseProduct(ProductEnum.SHIRT, 29.99, 1)

    );

    Receipt receipt = newYorkReceiptService.getReceipt(purchaseProductList);

    assertEquals(35.97, receipt.getSubTotal());

    assertEquals(0.55, DoubleRounder.round(receipt.getTax(), 2));

    assertEquals(36.52, DoubleRounder.round(receipt.getTotal(), 2));

  }

  @Test
  public void getReceipt_emptyListExceptionThrow() throws EmptyListException {

    assertThrows(EmptyListException.class, () -> newYorkReceiptService.getReceipt(null));

  }

}
