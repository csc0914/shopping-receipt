# Shopping Receipt

**Build project**
-------------------
```
mvn clean install
```

**Run project**
--------------------
Default will show the result of use case 1,2 and 3 in the console
```
mvn spring-boot:run
```

**Run Unit Test**
--------------------
```
mvn test
```

**Configuration**
--------------------
The configuration such as location,tax, product category and product can be configure in the Enum class

**Product Catgeory** - ProductCategoryEnum

**Location and Tax** - LocationEnum

**Product** - ProductEnum


**Create use case**
--------------------------
There is no user interface, command prompt for creating a use case, you are required to make it at the class ShoppingReceiptApplication,  you will see there 3 examples for use case 1, 2 and 3

